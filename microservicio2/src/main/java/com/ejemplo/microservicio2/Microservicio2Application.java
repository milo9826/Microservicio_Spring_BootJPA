package com.ejemplo.microservicio2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@ComponentScan("com.ejemplo.microserviciopaquete")
public class Microservicio2Application {

	public static void main(String[] args) {
		SpringApplication.run(Microservicio2Application.class, args);
	}

}
