package com.ejemplo.microserviciopaquete;



import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.mapeo.servicio.Actualizar_Aves;
import co.com.mapeo.servicio.ConsultarAves;
import co.com.mapeo.servicio.CrearAves;
import co.com.mapeo.servicio.EliminarAves;

@RestController
public class Microservicio {
	
    CrearAves create = new CrearAves();
    Actualizar_Aves actave = new Actualizar_Aves();
    EliminarAves elimave = new EliminarAves();
    ConsultarAves consuave = new ConsultarAves();
	@RequestMapping(method = RequestMethod.POST, path = "/insertar")
	public void insertar(@RequestParam(value = "codigoave") String codigoave,
			             @RequestParam(value = "nombre")String nombre,
			             @RequestParam(value = "nombrecientifico") String nombrecientifico){
		
	    create.insertarAves(codigoave, nombre, nombrecientifico);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/actualizar")
	public void actualizar(@RequestParam(value = "codigoave") String codigoave,
            @RequestParam(value = "nombre")String nombre,
            @RequestParam(value = "nombrecientifico") String nombrecientifico){

          
          actave.Actualizar(codigoave, nombre, nombrecientifico);
       }

	
	@RequestMapping(method = RequestMethod.POST, path = "/eliminar")
	public void eliminar(@RequestParam(value = "codigoave") String codigoave){

          
		elimave.eliminarAves(codigoave);
       }

	
	@RequestMapping(method = RequestMethod.GET, path = "/consultar")
	public List consultar(){

          
		return consuave.RetornarAves();
       }
}
