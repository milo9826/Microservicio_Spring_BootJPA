package co.com.mapeo.servicio;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import co.com.mapeo.entity.Aves;

public class CrearAves {
	   Aves aves = new Aves(); 
     public void insertarAves(String cdave, String nombre, String nombrecientifico) {
    	 EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "MapeoORM" );
         EntityManager entitymanager = emfactory.createEntityManager( );
         entitymanager.getTransaction( ).begin( );

      
         aves.setCdave(cdave);
         aves.setDsnombre_comun(nombre);
         aves.setDsnombre_cientifico(nombrecientifico);
         
         entitymanager.persist( aves );
         entitymanager.getTransaction( ).commit( );

         entitymanager.close( );
         emfactory.close( );
         
         
     } 

}
