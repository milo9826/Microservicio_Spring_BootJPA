package co.com.mapeo.servicio;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import co.com.mapeo.entity.Aves;

public class EliminarAves {
	
	Aves aves = new Aves();
	
	public  void eliminarAves(String cdave) 
	   {
	      EntityManagerFactory emfactory = Persistence.createEntityManagerFactory( "MapeoORM" );
	      EntityManager entitymanager = emfactory.createEntityManager( );
	      entitymanager.getTransaction( ).begin( );
	      
	      Aves aves = entitymanager.find( Aves.class, cdave );
	      entitymanager.remove( aves );
	      entitymanager.getTransaction( ).commit( );
	      entitymanager.close( );
	      emfactory.close( );
	   }

}
