package co.com.mapeo.servicio;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import co.com.mapeo.entity.Aves;

public class Actualizar_Aves {
    
	Aves aves = new Aves();
	
	public void Actualizar(String codigoave, String nombre, String nombrecientifico){
		
	      EntityManagerFactory emfactory = Persistence.
	      createEntityManagerFactory( "MapeoORM" );
	      EntityManager entitymanager = emfactory.createEntityManager( );
	      entitymanager.getTransaction( ).begin( );
	      Aves aves = entitymanager.find( Aves.class, codigoave );
	      
	      
	      
	      if((nombre.equals("") || nombre.equals(null)) && (nombrecientifico != "" && nombrecientifico != null)){
	    	  aves.setDsnombre_cientifico(nombrecientifico);
	    	  entitymanager.getTransaction( ).commit( );
	      }
	    	 
	      if((nombrecientifico.equals("") || nombrecientifico.equals(null)) && (nombre != "" || nombre != null)){
	    	  aves.setDsnombre_comun(nombre);
	    	  entitymanager.getTransaction( ).commit( );
	    	 
	    	  
	      }
	      
	      if((nombrecientifico != "" || nombrecientifico != null)  && (nombre != "" || nombre != null)){
	    	  aves.setDsnombre_comun(nombre);
	    	  aves.setDsnombre_cientifico(nombrecientifico);
	    	  entitymanager.getTransaction( ).commit( );
	    	 
	      }
	      entitymanager.close();
	      emfactory.close();

	   }
	
}
