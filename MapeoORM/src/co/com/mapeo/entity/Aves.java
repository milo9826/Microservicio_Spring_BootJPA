package co.com.mapeo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tont_aves")
public class Aves {

	 @Id
	 String cdave;
	 String dsnombre_comun;
	 String  dsnombre_cientifico;
	 
	public Aves() {
	
	}

	public Aves(String cdave, String dsnombre_comun, String dsnombre_cientifico) {
		this.cdave = cdave;
		this.dsnombre_comun = dsnombre_comun;
		this.dsnombre_cientifico = dsnombre_cientifico;
	}

	public String getCdave() {
		return cdave;
	}

	public void setCdave(String cdave) {
		this.cdave = cdave;
	}

	public String getDsnombre_comun() {
		return dsnombre_comun;
	}

	public void setDsnombre_comun(String dsnombre_comun) {
		this.dsnombre_comun = dsnombre_comun;
	}

	public String getDsnombre_cientifico() {
		return dsnombre_cientifico;
	}

	public void setDsnombre_cientifico(String dsnombre_cientifico) {
		this.dsnombre_cientifico = dsnombre_cientifico;
	}
	 
	 
	 
	 
	
}
