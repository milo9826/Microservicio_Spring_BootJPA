import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent implements OnInit{
  
  response: any; 


  constructor(private httpClient:HttpClient){ }
  ngOnInit() {
    this.httpClient.get('http://localhost:8080/consultar').subscribe(data => {
      this.response = data; 
      console.log(data);
    });
 }
  
}
